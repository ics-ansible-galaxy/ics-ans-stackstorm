import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('stackstorm')


def test_default(host):
    list_of_services = ["mongod.service", "nginx.service",
                        "rabbitmq-server.service", "redis.service",
                        "st2actionrunner.service", "st2api.service",
                        "st2auth.service", "st2garbagecollector.service",
                        "st2notifier.service", "st2rulesengine.service",
                        "st2rulesengine.service", "st2scheduler.service",
                        "st2sensorcontainer.service", "st2stream.service",
                        "st2timersengine.service", "st2workflowengine.service"]
    for item in list_of_services:
        st2 = host.service(item)
        assert st2.is_running
